class OSOInstruction():
    """
    
    """
    def __init__(self, line, tag):
        """
        
        :param line: 
        :param tag: 
        """
        tokens = line.split()
        self._opcode = tokens[0]
        self._tag = tag
        self._hints = []
        self._parameters = []
        for _id in range(1, len(tokens)):
            if tokens[_id][0] is '%':
                self._hints.extend([tokens[_id]])
            else:
                self._parameters.extend([tokens[_id]])


    '''--------------------------------Property API------------------------------------'''

    @property
    def opcode(self):
        return self._opcode
    @opcode.setter
    def opcode(self, value):
        self._opcode = value

    @property
    def tag(self):
        return self._tag
    @tag.setter
    def tag(self, value):
        self._tag = value

    @property
    def hints(self):
        return self._hints
    @hints.setter
    def hints(self, value):
        self._hints = value

    @property
    def parameters(self):
        return self._parameters
    @parameters.setter
    def parameters(self, value):
        self._parameters = value

    def __repr__(self):
        return "Instruction: %s(%s)" % (self.opcode, self.parameters)
